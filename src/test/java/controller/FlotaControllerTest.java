package controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import Models.Moto;
import Models.Trip;
import Models.Vehiculo;
import dao.DB;
import dao.FlotaDaoImpl;
import dao.IFlotaDao;
import service.FlotaServiceImpl;
import service.IFlotaService;

public class FlotaControllerTest {
	
	IFlotaDao flotaDaoImpl = new FlotaDaoImpl();
	IFlotaService flotaServiceImpl = new FlotaServiceImpl(flotaDaoImpl );
	IFlotaController flotaController = new FlotaControllerImpl(flotaServiceImpl);
	
	@Test
	public void CanAddVehiculoToRepository() {
		
		Vehiculo vehiculo = (new Moto(002, "SLP-256", 50, 50)) ;
		
		flotaController.addVehiculoToRepository(vehiculo);

		Assert.assertEquals(DB.vehiculos.get(0).getSeriesNum(), 002);
		flotaController.deleteVehiculoFromRepository(002);
	}
	
	@Test
	public void CanDeleteVehiculoFromRepositoryWithSeriesNumber() {
		//given
		Vehiculo vehiculo = (new Moto(005, "SLP-256", 50, 50)) ;

		//when
		flotaController.addVehiculoToRepository(vehiculo);
		flotaController.deleteVehiculoFromRepository(005);
		
		//then
		assertEquals(flotaController.getVehiculoWithSeriesNum(005), null);
	}
	
	@Test
	public void CanAddTripToVehiculo() {
		//given
		Vehiculo vehiculo = (new Moto(005, "SLP-256", 50, 50));
		
		//when
		flotaController.addVehiculoToRepository(vehiculo);
		Trip trip = new Trip(1, 30);

		flotaController.addTripToVehiculo(DB.vehiculos.get(0),trip);
		
		//then
		Assert.assertEquals(DB.vehiculos.get(0).getTrips().get(0).getID(), 1);
		flotaController.deleteVehiculoFromRepository(005);
	}
	
	@Test
	public void CanFinishTrip() {
		//given
		Vehiculo vehiculo = (new Moto(005, "SLP-256", 50, 50));
		//when
		flotaController.addVehiculoToRepository(vehiculo);
		flotaController.addTripToVehiculo(vehiculo,new Trip(1, 30));

		SimpleDateFormat finishTrip = flotaController.finishTrip(005, 1);
		flotaController.deleteVehiculoFromRepository(005);

		//then
		assertTrue(!(finishTrip == null));
	}
	
	@Test
	public void CanReturnVehiculoWithSeriesNumber() {
		//given
		flotaController.addVehiculoToRepository(new Moto(003, "SLP-256", 40, 40));

		//when
		Vehiculo vehiculo = flotaController.getVehiculoWithSeriesNum(003);
		
		//then
		flotaController.deleteVehiculoFromRepository(003);
		assertThat(vehiculo, is(new Moto(003, "SLP-256", 40, 40)));
	}
	
	@Test
	public void CanReturnAllVehiculos() {
		
		//given
		flotaController.addVehiculoToRepository(new Moto(001, "SLP-256", 40, 40));

		//when
		List<Vehiculo> actual = flotaController.getAllVehiculos();
		List<Vehiculo> expected = Arrays.asList(new Moto(001, "SLP-256", 40, 40));

		//then
		assertThat(actual, is(expected));
	}
	
	@Test
	public void CanGetTotalConsumoFromVehiculos () {
		//given
		Vehiculo vehiculo = (new Moto(005, "SLP-256", 50, 50));
				
		//when
		flotaController.addVehiculoToRepository(vehiculo);
		Trip trip = new Trip(1, 30);

		flotaController.addTripToVehiculo(DB.vehiculos.get(0),trip);
		double totalConsumoFromVehiculos = flotaController.getTotalConsumoFromVehiculos();

		//then
		flotaController.deleteVehiculoFromRepository(005);
		Assert.assertEquals(totalConsumoFromVehiculos, 15d, 0);
	}
}
