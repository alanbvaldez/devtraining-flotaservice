package service;

import java.text.SimpleDateFormat;
import java.util.List;

import Models.Trip;
import Models.Vehiculo;

public interface IFlotaService {

	void addVehiculoToRepository(Vehiculo vehiculo);

	void addTripToVehiculo(Vehiculo vehiculo, Trip trip);

	List<Vehiculo> getAllVehiculos();

	Vehiculo getVehiculoWithSeriesNum(int seriesNum);

	SimpleDateFormat finishTrip(int vehicleId, int tripId);

	void deleteVehiculoFromRepository(int seriesNum);

	double getTotalConsumoFromVehiculos();
	
}
