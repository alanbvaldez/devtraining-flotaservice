package service;

import java.text.SimpleDateFormat;
import java.util.List;

import Models.Trip;
import Models.Vehiculo;
import dao.DB;
import dao.IFlotaDao;

public class FlotaServiceImpl implements IFlotaService {

	IFlotaDao flotaDao;
	
	public FlotaServiceImpl(IFlotaDao flotaDao) {
		super();
		this.flotaDao = flotaDao;
	}

	@Override
	public void addVehiculoToRepository(Vehiculo vehiculo) {
		flotaDao.addVehiculoToRepository(vehiculo);
	}


	@Override
	public void addTripToVehiculo(Vehiculo vehiculo, Trip trip) {
		for(int i = 0; i < DB.vehiculos.size(); i++) {
			if(DB.vehiculos.get(i).getSeriesNum() == vehiculo.getSeriesNum()) {
				flotaDao.addTripToVehiculo(i, trip);
				break;
			}
		}
	}

	@Override
	public List<Vehiculo> getAllVehiculos() {
		return flotaDao.getAllVehiculo();
		
	}

	@Override
	public Vehiculo getVehiculoWithSeriesNum(int seriesNum) {
		return flotaDao.getVehiculoWithSeriesNum(seriesNum);
	}

	@Override
	public SimpleDateFormat finishTrip(int vehicleId, int tripId) {
		return flotaDao.finishTrip(vehicleId, tripId);
	}

	@Override
	public void deleteVehiculoFromRepository(int seriesNum) {
		flotaDao.deleteVehiculoFromRepository(seriesNum);
	}

	@Override
	public double getTotalConsumoFromVehiculos() {
		return flotaDao.getTotalConsumoFromVehiculos();
	}
}
