package Models;

import java.util.ArrayList;
import java.util.List;

public abstract class Vehiculo {
	
	private int seriesNum;
	private String licensePlate;
	private double gasCapacity;
	private double gasAmount;
	private List<Trip> trips = new ArrayList<>();
	
	
	
	public Vehiculo(int seriesNum, String licensePlate, double gasCapacity, double gasAmount) {
		super();
		this.seriesNum = seriesNum;
		this.licensePlate = licensePlate;
		this.gasCapacity = gasCapacity;
		this.gasAmount = gasAmount;
	}

	public int getSeriesNum() {
		return seriesNum;
	}
	
	public String getLicensePlate() {
		return licensePlate;
	}
	
	public double getGasCapacity() {
		return gasCapacity;
	}
	
	public double getGasAmount() {
		return gasAmount;
	}
	
	public List<Trip> getTrips() {
		return trips;
	}
	
	public void addTrip(Trip trip) {
		this.trips.add(trip);
	}
	
	public abstract Double getConsumo();

	@Override
	public String toString() {
		return "Vehiculo [seriesNum=" + seriesNum + ", licensePlate=" + licensePlate + ", gasCapacity=" + gasCapacity
				+ ", gasAmount=" + gasAmount + ", trips=" + trips + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(gasAmount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(gasCapacity);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((licensePlate == null) ? 0 : licensePlate.hashCode());
		result = prime * result + seriesNum;
		result = prime * result + ((trips == null) ? 0 : trips.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehiculo other = (Vehiculo) obj;
		if (Double.doubleToLongBits(gasAmount) != Double.doubleToLongBits(other.gasAmount))
			return false;
		if (Double.doubleToLongBits(gasCapacity) != Double.doubleToLongBits(other.gasCapacity))
			return false;
		if (licensePlate == null) {
			if (other.licensePlate != null)
				return false;
		} else if (!licensePlate.equals(other.licensePlate))
			return false;
		if (seriesNum != other.seriesNum)
			return false;
		if (trips == null) {
			if (other.trips != null)
				return false;
		} else if (!trips.equals(other.trips))
			return false;
		return true;
	}
	
	
}
