package Models;

public class Moto extends Vehiculo {
	//consume 0.5 litros de gasolina por cada kilometro
	public static final double EFICIENCIA = 0.5; 

	public Moto(int seriesNum, String licensePlate, double gasCapacity, double gasAmount) {
		super(seriesNum, licensePlate, gasCapacity, gasAmount);
	}
	
	@Override
	public Double getConsumo() {
		return super.getTrips().stream().mapToDouble(trip -> trip.getRouteTotalKM() * EFICIENCIA).sum();
	}

}
