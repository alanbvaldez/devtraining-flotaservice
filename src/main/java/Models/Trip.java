package Models;

import java.text.SimpleDateFormat;

public class Trip {

	private final int id;
	private SimpleDateFormat startDate;
	private SimpleDateFormat finishDate;
	private double routeTotalKM;
	
	public Trip(int id, double routeTotalKM) {
		super();
		this.startDate = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
		this.id = id;
		this.routeTotalKM = routeTotalKM;

	}
	
	public int getID() {
		return id;
	}
	
	public SimpleDateFormat getStartDate() {
		return startDate;
	}
	
	public SimpleDateFormat getFinishDate() {
		return finishDate;
	}
	public void isFinishDate() {
		this.finishDate  = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");;
	}
	public double getRouteTotalKM() {
		return routeTotalKM;
	}

	@Override
	public String toString() {
		return "Trip [id=" + id + ", startDate=" + startDate + ", finishDate=" + finishDate + ", routeTotalKM="
				+ routeTotalKM + "]";
	}
}
