package controller;


import java.text.SimpleDateFormat;
import java.util.List;

import Models.Trip;
import Models.Vehiculo;
import service.IFlotaService;

public class FlotaControllerImpl implements IFlotaController {

	IFlotaService flotaService;
	
	
	public FlotaControllerImpl(IFlotaService flotaService) {
		super();
		this.flotaService = flotaService;
	}


	@Override
	public void addVehiculoToRepository(Vehiculo vehiculo) {
		flotaService.addVehiculoToRepository(vehiculo);
	}


	@Override
	public void addTripToVehiculo(Vehiculo vehiculo, Trip trip) {
		// TODO Auto-generated method stub
		flotaService.addTripToVehiculo(vehiculo, trip);
	}


	@Override
	public List<Vehiculo> getAllVehiculos() {
		return flotaService.getAllVehiculos();
	}


	@Override
	public Vehiculo getVehiculoWithSeriesNum(int seriesNum) {
		return flotaService.getVehiculoWithSeriesNum(seriesNum);
	}


	@Override
	public SimpleDateFormat finishTrip(int vehicleId, int tripId) {
		return flotaService.finishTrip(vehicleId, tripId);
	}


	@Override
	public void deleteVehiculoFromRepository(int seriesNum) {
		flotaService.deleteVehiculoFromRepository(seriesNum);
	}


	@Override
	public double getTotalConsumoFromVehiculos() {
		return flotaService.getTotalConsumoFromVehiculos();
	}

}
