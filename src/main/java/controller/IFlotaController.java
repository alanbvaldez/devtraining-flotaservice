package controller;

import java.text.SimpleDateFormat;
import java.util.List;

import Models.Trip;
import Models.Vehiculo;

public interface IFlotaController {

	void addVehiculoToRepository(Vehiculo vehiculo);

	void addTripToVehiculo(Vehiculo vehiculo, Trip trip);

	List<Vehiculo> getAllVehiculos();

	Vehiculo getVehiculoWithSeriesNum(int i);

	SimpleDateFormat finishTrip(int vehicleId, int tripId);

	void deleteVehiculoFromRepository(int i);

	double getTotalConsumoFromVehiculos();

	
}
