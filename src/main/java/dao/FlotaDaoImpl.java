package dao;

import java.text.SimpleDateFormat;
import java.util.List;

import Models.Trip;
import Models.Vehiculo;

public class FlotaDaoImpl implements IFlotaDao {

	@Override
	public void addVehiculoToRepository(Vehiculo vehiculo) {
		DB.vehiculos.add(vehiculo);
	}

	@Override
	public void addTripToVehiculo(int index, Trip trip) {
		DB.vehiculos.get(index).addTrip(trip);	
	}

	@Override
	public List<Vehiculo> getAllVehiculo() {
		return DB.vehiculos;
	}

	@Override
	public Vehiculo getVehiculoWithSeriesNum(int seriesNum) {
		 for(int i = 0; i < DB.vehiculos.size(); i++) {
			if(DB.vehiculos.get(i).getSeriesNum() == seriesNum) {
				return DB.vehiculos.get(i);
			}
		}
		return null;
	}

	@Override
	public SimpleDateFormat finishTrip(int seriesNum, int tripId) {
		for(int i = 0; i < DB.vehiculos.size(); i++) {
			if(DB.vehiculos.get(i).getSeriesNum() == seriesNum) {
				for (int j = 0; j < DB.vehiculos.get(i).getTrips().size(); j++) {
					if (DB.vehiculos.get(i).getTrips().get(i).getID() == tripId) {
						DB.vehiculos.get(i).getTrips().get(i).isFinishDate();
						return DB.vehiculos.get(i).getTrips().get(i).getFinishDate();
					}
				}
			}
		}
		return null;		
	}

	@Override
	public void deleteVehiculoFromRepository(int seriesNum) {
		for(int i = 0; i < DB.vehiculos.size(); i++) {
			if(DB.vehiculos.get(i).getSeriesNum() == seriesNum) {
				DB.vehiculos.remove(i);
			}
		}
	}

	@Override
	public double getTotalConsumoFromVehiculos() {
		double totalConsumo = 0;
				
		for (Vehiculo vehiculo : DB.vehiculos) {
			totalConsumo += vehiculo.getConsumo();
		}
		
		return totalConsumo;
	}
}
