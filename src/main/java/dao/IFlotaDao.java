package dao;

import java.text.SimpleDateFormat;
import java.util.List;

import Models.Trip;
import Models.Vehiculo;

public interface IFlotaDao {

	void addVehiculoToRepository(Vehiculo vehiculo);

	void addTripToVehiculo(int i, Trip trip);

	List<Vehiculo> getAllVehiculo();

	Vehiculo getVehiculoWithSeriesNum(int seriesNum);

	SimpleDateFormat finishTrip(int vehicleId, int tripId);

	void deleteVehiculoFromRepository(int seriesNum);

	double getTotalConsumoFromVehiculos();
}
